# chef-tuxedo cookbook

## Requirements

### Chef
Tested on 11.6.0

### Platform
The following platforms have been tested with this cookbook, meaning that the
recipes run on these platforms without error:
* rhel
* centos

## Attributes
* node["tuxedo"]["home"]      installation destination
* node["tuxedo"]["installer"]["url"]    remote location of installer package

## Recipes
* default    - silently install tuxedo with patch
* install    - silently install tuxedo
* patch      - silently install patch

## License and Author

* Author: Luke Bradbury (<luke.bradbury@derby.ac.uk>)
* Copyright: 2013, University of Derby

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

