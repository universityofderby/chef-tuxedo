default["oracle"]["middleware"]["home"] = "/usr/local/oracle/Middleware"
default["tuxedo"]["user"]  = 'tuxedo'
default["tuxedo"]["group"] = 'tuxedo'
default["tuxedo"]["home"]  =  ::File.join node["oracle"]["middleware"]["home"], "tuxedo"
#Server,Client,ATMI,CORBA,Jolt or Full
default["tuxedo"]["locale"] = 'en'
default["tuxedo"]["install_set"] = 'Full'
default["tuxedo"]["install_samples"] = true
default["tuxedo"]["tlisten_password"] = 'luckluck'
