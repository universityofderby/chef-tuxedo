# Author:: Luke Bradbury (<luke.bradbury@derby.ac.uk>)
# Cookbook Name:: tuxedo
# Recipe:: patch
#
# Copyright 2013 University of Derby
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and

include_recipe 'tuxedo::install'

chef_gem 'greenletters'
require 'greenletters'

source_file = node["tuxedo"]["patch"]["url"].split("/")[-1]
tmp_dir         = File.join Chef::Config[:file_cache_path], 'tuxedo'
installer_file  = File.join tmp_dir, source_file

directory tmp_dir do
  user  node['tuxedo']['user']
  group node['tuxedo']['group']
  mode  02755
end

ark source_file do
  url node["tuxedo"]["patch"]["url"]
  path tmp_dir
  action :put
end

ruby_block 'patch' do
  block do
    def find_version(file)
      if File.exists? file
        lines = IO.readlines(file)
        version = /\d*/.match(lines.last)[0].to_i
      else
        version = 0
      end
    end
    installed_patch = File.join node["tuxedo"]["home"], "udataobj", "patchlev"
    proposed_patch  = File.join installer_file, "udataobj", "patchlev"
    installed_version = find_version installed_patch
    proposed_version  = find_version proposed_patch
    if proposed_version > installed_version
      cwd = installer_file
      command = "sh install"
      env = { 'TUXDIR' => node['tuxedo']['home'] }
      timeout = 60
      install = Greenletters::Process.new(command, {cwd: cwd, transcript: $stdout, env: env, timeout: timeout})
      install.start!
      install.wait_for(:output,/owner/i)
      install << "#{node['tuxedo']['user']}\n"
      install.wait_for(:output,/group/i)
      install << "#{node['tuxedo']['group']}\n"
      install.wait_for(:exit)
    end
  end
  notifies :run, 'execute[permissions]'
end

execute 'permissions' do
  command "chmod -R 02755 #{node['tuxedo']['home']}"
  action :nothing
end
