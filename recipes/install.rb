# Author:: Luke Bradbury (<luke.bradbury@derby.ac.uk>)
# Cookbook Name:: tuxedo
# Recipe:: install
#
# Copyright 2013 University of Derby
#
# Licensed under the Apache License, Version 2.0 (the 'License');
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an 'AS IS' BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and

source_file     = node['tuxedo']['installer']['url'].split('/')[-1]
tmp_dir         = File.join Chef::Config[:file_cache_path], 'tuxedo'
installer_file  = File.join tmp_dir, source_file
answer_file     = File.join tmp_dir, 'installer.properties'
tux_env         = File.join node['tuxedo']['home'], 'tux.env'

group node['tuxedo']['group']

user node['tuxedo']['user'] do
  gid node['tuxedo']['group']
end

directory node['tuxedo']['home'] do
  user  node['tuxedo']['user']
  group node['tuxedo']['group']
  mode  02755
  recursive true
end

directory tmp_dir do
  user  node['tuxedo']['user']
  group node['tuxedo']['group']
  mode  02755
end

template answer_file do
  mode 00750
  source 'installer.properties.erb'
end

remote_file installer_file do
  user  node['tuxedo']['user']
  group node['tuxedo']['group']
  mode  00750
  source node['tuxedo']['installer']['url']
end

execute 'install' do
  user  node['tuxedo']['user']
  group node['tuxedo']['group']
  cwd   tmp_dir
  environment 'IATEMPDIR' => tmp_dir
  command "sh #{installer_file} -f #{answer_file}"
  not_if { File.exists? tux_env }
  notifies :run, 'execute[permissions]'
end

execute 'permissions' do
  command "chmod -R 02755 #{node['tuxedo']['home']}"
  action :nothing
end
